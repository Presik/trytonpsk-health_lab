# -*- coding: utf-8 -*-
from datetime import datetime
from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateTransition, StateView, Button
from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond.i18n import gettext
from trytond.exceptions import UserWarning


class CreateLabTestOrderInit(ModelView):
    'Create Test Report Init'
    __name__ = 'health.lab.test.create.init'


class CreateLabTestOrder(Wizard):
    'Create Lab Test Report'
    __name__ = 'health.lab.test.create'

    start = StateView('health.lab.test.create.init',
        'health_lab.view_lab_make_test', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Create Test Order', 'create_lab_test', 'tryton-ok', True),
        ])
    create_lab_test = StateTransition()

    def transition_create_lab_test(self):
        pool = Pool()
        TestRequest = pool.get('health.patient.lab.test')
        Lab = pool.get('health.lab')
        tests_report_data = []
        tests = TestRequest.browse(Transaction().context.get('active_ids'))

        for lab_test_order in tests:
            test_cases = []
            test_report_data = {}
            if lab_test_order.state == 'ordered':
                raise UserWarning(gettext(
                    "health_lab.msg_test_order_already_created"
                ))

            test_report_data['test'] = lab_test_order.name.id
            test_report_data['patient'] = lab_test_order.patient.id
            if lab_test_order.doctor_id:
                test_report_data['requestor'] = lab_test_order.doctor.id
            test_report_data['date_requested'] = lab_test_order.date
            test_report_data['request_order'] = lab_test_order.request

            for critearea in lab_test_order.name.critearea:
                test_cases.append(('create', [{
                    'name': critearea.name,
                    'sequence': critearea.sequence,
                    'lower_limit': critearea.lower_limit,
                    'upper_limit': critearea.upper_limit,
                    'normal_range': critearea.normal_range,
                    'units': critearea.units and critearea.units.id,
                }]))
            test_report_data['critearea'] = test_cases

            tests_report_data.append(test_report_data)

        Lab.create(tests_report_data)
        TestRequest.write(tests, {'state': 'ordered'})

        return 'end'


class RequestTest(ModelView):
    'Request - Test'
    __name__ = 'health.request-test'
    _table = 'health_request_test'

    request = fields.Many2One('health.patient.lab.test.request.start',
        'Request', required=True)
    test = fields.Many2One('health.lab.test_type', 'Test', required=True)


class RequestPatientLabTestStart(ModelView):
    'Request Patient Lab Test Start'
    __name__ = 'health.patient.lab.test.request.start'

    date = fields.DateTime('Date')
    patient = fields.Many2One('health.patient', 'Patient', required=True)
    doctor = fields.Many2One('health.professional', 'Doctor',
        help="Doctor who Request the lab tests.")
    tests = fields.Many2Many('health.request-test', 'request', 'test',
        'Tests', required=True)
    urgent = fields.Boolean('Urgent')

    @staticmethod
    def default_date():
        return datetime.now()

    @staticmethod
    def default_patient():
        if Transaction().context.get('active_model') == 'health.patient':
            return Transaction().context.get('active_id')

    @staticmethod
    def default_doctor():
        return Pool().get('health.professional').get_health_professional()


class RequestPatientLabTest(Wizard):
    'Request Patient Lab Test'
    __name__ = 'health.patient.lab.test.request'

    start = StateView('health.patient.lab.test.request.start',
        'health_lab.patient_lab_test_request_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Request', 'request', 'tryton-ok', default=True),
        ])
    request = StateTransition()

    def transition_request(self):
        pool = Pool()
        PatientLabTest = pool.get('health.patient.lab.test')
        config = pool.get('health.configuration').get_config()
        request_number = config.lab_request_sequence.get_id()
        lab_tests = []
        for test in self.start.tests:
            lab_test = {}
            lab_test['request'] = request_number
            lab_test['name'] = test.id
            lab_test['patient_id'] = self.start.patient.id
            if self.start.doctor:
                lab_test['doctor_id'] = self.start.doctor.id
            lab_test['date'] = self.start.date
            lab_test['urgent'] = self.start.urgent
            lab_tests.append(lab_test)
        PatientLabTest.create(lab_tests)
        return 'end'
