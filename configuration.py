# -*- coding: utf-8 -*-
from trytond.model import fields
from trytond.pool import PoolMeta


class HealthConfiguration(metaclass=PoolMeta):
    "Health Configuration"
    __name__ = "health.configuration"

    lab_sequence = fields.Many2One('ir.sequence', 'Lab Sequence',
        required=True)
    lab_request_sequence = fields.Many2One('ir.sequence',
        'Patient Lab Request Sequence', required=True)
