# -*- coding: utf-8 -*-
from trytond.modules.company import CompanyReport


class LabTestReport(CompanyReport):
    __name__ = 'patient.labtest.report'
